﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TxtConsoleApp.Model;
using TxtConsoleApp.Repository;
using TxtConsoleApp.Config;
using System.IO;

namespace TxtConsoleApp.Controller
{
    public class InfoController
    {
        const string fileKoGhar = @"C:\Users\PercoidIT\Downloads\TxtConsoleApp\Data.txt";

        StudentRepository _stdRepo = new StudentRepository();
        
        public void Functionality()
        {
            while (true)
            {
                DisplayMainMenu();

                int codeNumber = Convert.ToInt32(Console.ReadLine());

                switch (codeNumber)
                {
                    case 1:

                        InsertStudents();
                        break;

                    case 2:
                        DataLoader(fileKoGhar);
                        break;

                    case 3:
                        Update();
                        break;

                    case 4:
                        Delete();
                        break;

                    case 5:
                        Environment.Exit(0);
                        break;
                }
            }
        }

        public void DataLoader(string fileKoGhar)
        {
            DataLoader dl = new DataLoader();
            dl.StudentRepository = _stdRepo;
            dl.ReadDataFromFile(fileKoGhar);
            foreach(var item in dl._StudentList)
            {
                Console.WriteLine(item.Id + " " + item.Name + " " + item.Address + " " + item.Email);
            }

            
        }

        public void DisplayMainMenu()
        {
            Console.WriteLine("-------------------------STUDENT DIARY-------------------------");
            Console.WriteLine("\n");
            Console.WriteLine("1-ADD STUDENT");
            Console.WriteLine("2-DISPLAY STUDENTS");
            Console.WriteLine("3-UPDATE STUDENT ");
            Console.WriteLine("4-DELETE STUDENT");
            Console.WriteLine("5-EXIT");
            Console.WriteLine("\n");
            Console.Write("Enter Code Number: "+ "\n");
        }

        public void InsertStudents()
        {
            
            Console.WriteLine("ID: ");
            int Id = Convert.ToInt32(Console.ReadLine());

            Student students = _stdRepo.GetById(Id);

            if(students == null)
            {
                Student s = new Student();
                s.Id = Id;

                Console.WriteLine("Name: ");
                s.Name = Console.ReadLine();

                Console.WriteLine("Address: ");
                s.Address = Console.ReadLine();

                Console.WriteLine("Email: ");
                s.Email = Console.ReadLine();

                _stdRepo.IInsert(s);
                WriteFile();

            }
            else
            {
                Console.WriteLine("Id already exists. Try agian");
            }

            /*
            Console.WriteLine("ID: ");
            int Id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Name: ");
            string Name = Console.ReadLine();

            Console.WriteLine("Address: ");
            string Address = Console.ReadLine();

            Console.WriteLine("Email: ");
            string Email = Console.ReadLine();

            Student myStudent = new Student();

            myStudent.id = Id;
            myStudent.Name = Name;
            myStudent.Address = Address;
            myStudent.Email = Email;

            StreamWriter sw = File.AppendText(fileKoGhar);

            sw.WriteLine(myStudent.id + "," + myStudent.Name+ "," + myStudent.Address + "," + myStudent.Email);
            sw.Close();*/
            
        }

        public void DisplayAll()
        {
            string data;

            try
            {
                StreamReader reader = new StreamReader(fileKoGhar);
                data = reader.ReadLine();

                while (data != null)
                {
                    Console.WriteLine(data);
                    data = reader.ReadLine();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            /*
            string[] lines = File.ReadAllLines(fileKoGhar);
            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }*/
        }

        public void Update()
        {

        }

        public void Delete()
        {
            
            Console.WriteLine("Enter the number that you want to delete");
            int Id = Convert.ToInt32(Console.ReadLine());

            var student = _stdRepo.GetById(Id);
            if (student != null)
            {
                _stdRepo.Delete(student);
            }
         
            //Console.WriteLine("Enter the ID you want to Delete");
            //int deleteId = Convert.ToInt32(Console.ReadLine());
            /*
            Console.WriteLine("Enter the ID you want to Delete");
            int deleteId = Convert.ToInt32(Console.ReadLine());

            var students = _stdRepo.GetById(deleteId);
            if(students != null)
            {
                _stdRepo.Delete(students);
            }*/
        }


        public void WriteFile()
        {
            StreamWriter sw = new StreamWriter(fileKoGhar);
            foreach (Student s in _stdRepo._StudentListToInsert)
            {
                sw.WriteLine(s.ToCSV());
            }
            sw.Close();
        }
    }
}
