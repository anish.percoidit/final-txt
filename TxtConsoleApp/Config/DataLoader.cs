﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TxtConsoleApp.Model;
using TxtConsoleApp.Repository;
using TxtConsoleApp.Util;


namespace TxtConsoleApp.Config
{
    public class DataLoader
    {
        public IStudentRepository StudentRepository { get; set; }

        public IList<Student> _StudentList = new List<Student>();

        public void LoadData(string file)
        {
            foreach(string line in Importer.ReadLines(file))
            {
                string[] tokens = line.Split(",".ToCharArray());
                int Id = Convert.ToInt32(tokens[0]);
                Student students = StudentRepository.GetById(Id);

                if(students != null)
                {
                    _StudentList.Add(MapStudentsData(tokens));
                }
            }
            foreach (Student s in _StudentList)
            {
                Student students = StudentRepository.GetById(s.Id);
                if (students == null)
                {
                    StudentRepository.IInsert(s);
                }
            }
        }

        public void ReadDataFromFile(string file)
        {
            foreach (string line in Importer.ReadLines(file))
            {
                string[] tokens = line.Split(",".ToCharArray());
                Student students = MapStudentsData(tokens);
                _StudentList.Add(students);
            }
        }

        public Student MapStudentsData(string[] tokens)
        {
            Student myStudent = new Student();
            myStudent.Id = Convert.ToInt32(tokens[0]);
            myStudent.Name = tokens[1];
            myStudent.Address = tokens[2];
            myStudent.Email = tokens[3];
            return myStudent;
        }
    }
}
