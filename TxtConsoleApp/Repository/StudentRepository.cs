﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TxtConsoleApp.Repository;
using TxtConsoleApp.Controller;
using TxtConsoleApp.Model;
using System.IO;
using TxtConsoleApp.Util;

namespace TxtConsoleApp.Repository
{
    public interface IStudentRepository
    {
        void IInsert(Student students);
        Student GetById(int id);
        void Delete(Student students);
        IList<Student> GetAll();
    }

    public class StudentRepository : IStudentRepository
    {
        public IList<Student> _StudentList = new List<Student>();

        public IList<Student> _StudentListToInsert = new List<Student>();

        public IList<Student> GetAll()
        {
            string filepath = @"C:\Users\PercoidIT\Downloads\TxtConsoleApp\Data.txt";
            IList<string> abcd = Importer.ReadLines(filepath);
            foreach(string s in abcd)
            {
                string[] smth = s.Split(",".ToCharArray());
                Student myStudents = new Student();
                myStudents.Id = Convert.ToInt32(smth[0]);
                myStudents.Name = smth[1];
                myStudents.Address = smth[2];
                myStudents.Email = smth[3];
                _StudentList.Add(myStudents);
            }
            return _StudentList;
        }

        public void IInsert(Student students)
        {
            _StudentListToInsert.Add(students);
        }

        public Student GetById(int Id)
        {
            foreach(Student s in _StudentList)
            {
                if (s.Id == Id)
                {
                    return s;
                }
            }
            return null;
        }

        public void Delete(Student students)
        {
            _StudentList.Remove(students);
        }

        /*
        private void Display(Student students)
        {
            Console.WriteLine($"ID: {students.id}, STUDENT: {students.Name}, SYMBOL: {students.Address}, EMAIL: {students.Email}");
        }

        public void DisplayAll()
        {
            foreach (var person in _StudentList)
            {
                Display(person);
            }
        }*/
    }
}
