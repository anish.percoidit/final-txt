﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TxtConsoleApp.Repository;
using TxtConsoleApp.Controller;
using TxtConsoleApp.Model;

using System.IO;


namespace TxtConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            InfoController controllerObj = new InfoController();
            controllerObj.Functionality();
        }
    }
}
